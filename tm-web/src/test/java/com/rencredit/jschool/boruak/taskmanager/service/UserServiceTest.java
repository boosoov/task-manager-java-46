package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Before
    public void setup() throws EmptyLoginException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyPasswordException, EmptyUserException, DeniedAccessException {
        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        SecurityContextHolder.setContext(ctx);
        ctx.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "", Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"))));

        userService.deleteAll();
    }

    @Test
    public void testSaveLoginPassword() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, DeniedAccessException, EmptyPasswordException {
        userService.save("login", "password");
        assertNotNull(userService.findByLoginDTO("login"));
    }

    @Test
    public void testSaveLoginPasswordRole() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException {
        userService.save("login", "password", Role.ADMIN);
        final UserDTO userFromBase = userService.findByLoginDTO("login");
        assertEquals(Role.ADMIN, userFromBase.getRole());
    }

    @Test
    public void testSaveUser() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException {
        final User user = new User("login", "password");
        userService.save(user);
        assertNotNull(userService.findByIdDTO(user.getId()));
    }

    @Test
    public void testSaveUserDTO() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException {
        final UserDTO user = new UserDTO("login", "password");
        userService.save(user);
        assertNotNull(userService.findByIdDTO(user.getId()));
    }

    @Test
    public void testRewrite() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, UnknownUserException {
        final UserDTO user = new UserDTO("login", "password");
        userService.save(user);
        assertNotNull(userService.findByIdDTO(user.getId()));
        user.setLogin("login2");
        userService.rewrite(user);
        final UserDTO userFromBase = userService.findByIdDTO(user.getId());
        assertEquals("login2", userFromBase.getLogin());
    }

    @Test
    public void testFindByIdDTO() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException {
        final User user = new User("login", "password");
        userService.save(user);
        final UserDTO userFromBase = userService.findByIdDTO(user.getId());
        assertEquals("login", userFromBase.getLogin());
    }

    @Test
    @Transactional
    public void testFindByIdEntity() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException {
        final User user = new User("login", "password");
        userService.save(user);
        final User userFromBase = userService.findByIdEntity(user.getId());
        assertEquals("login", userFromBase.getLogin());
    }

    @Test
    public void testFindByLoginDTO() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException {
        final User user = new User("login", "password");
        userService.save(user);
        final UserDTO userFromBase = userService.findByLoginDTO(user.getLogin());
        assertEquals(user.getId(), userFromBase.getId());
    }

    @Test
    public void testFindByLoginEntity() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException {
        final User user = new User("login", "password");
        userService.save(user);
        final User userFromBase = userService.findByLoginEntity(user.getLogin());
        assertEquals(user.getId(), userFromBase.getId());
    }

    @Test
    public void testFindListDTO() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException {
        userService.save("login1", "password");
        userService.save("login2", "password");
        userService.save("login3", "password");
        assertEquals(6, userService.findListDTO().size());
    }

    @Test
    public void testUpdatePasswordById() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException, EmptyNewPasswordException, IncorrectHashPasswordException {
        final User user = new User("login", "password");
        userService.save(user);
        final User userFromBase = userService.findByLoginEntity(user.getLogin());
        final String firstHash = userFromBase.getPasswordHash();

        userService.updatePasswordById(user.getId(), "password2");
        final User userFromBaseWithNewPassword = userService.findByLoginEntity(user.getLogin());
        final String secondHash = userFromBaseWithNewPassword.getPasswordHash();

        assertNotEquals(firstHash, secondHash);
    }

    @Test
    public void testLockUserByLogin() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException {
        final User user = new User("login", "password");
        userService.save(user);
        assertFalse(user.isLocked());
        userService.lockUserByLogin(user.getLogin());
        final User userFromBase = userService.findByLoginEntity(user.getLogin());
        assertTrue(userFromBase.isLocked());
    }

    @Test
    public void testUnlockUserByLogin() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException {
        final User user = new User("login", "password");
        userService.save(user);
        assertFalse(user.isLocked());

        userService.lockUserByLogin(user.getLogin());
        final User userFromBaseLock = userService.findByLoginEntity(user.getLogin());
        assertTrue(userFromBaseLock.isLocked());

        userService.unlockUserByLogin(user.getLogin());
        final User userFromBaseUnlock = userService.findByLoginEntity(user.getLogin());
        assertFalse(userFromBaseUnlock.isLocked());
    }

    @Test
    public void testDeleteByLogin() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException {
        final User user = new User("login", "password");
        userService.save(user);
        userService.deleteByLogin(user.getLogin());
        assertNull(userService.findByIdDTO(user.getId()));
    }

    @Test
    public void testDeleteById() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException {
        final User user = new User("login", "password");
        userService.save(user);
        userService.deleteById(user.getId());
        assertNull(userService.findByIdDTO(user.getId()));
    }

    @Test
    public void testDeleteByUser() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException {
        final UserDTO user = new UserDTO("login", "password");
        userService.save(user);
        userService.deleteByUserDTO(user);
        assertNull(userService.findByIdDTO(user.getId()));
    }

    @Test
    public void testDeleteAll() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException, EmptyRoleException, DeniedAccessException {
        userService.save("login1", "password");
        userService.save("login2", "password");
        userService.deleteAll();
        assertEquals(3, userService.findListDTO().size());
    }

}
