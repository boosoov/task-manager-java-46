package com.rencredit.jschool.boruak.taskmanager.repository.dto;

import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.IUserRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IUserRepositoryEntity;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class UserRepositoryDtoTest {

    @Autowired
    private IUserRepositoryDTO userRepository;

    @Autowired
    private IUserRepositoryEntity userRepositoryEntity;

    @Before
    public void setup() {
        userRepositoryEntity.deleteAll();
    }

    @Test
    public void testSave() {
        final UserDTO user = new UserDTO("login", "password");
        userRepository.save(user);
        assertNotNull(userRepository.findById(user.getId()));
    }

    @Test
    public void testFindById() {
        final UserDTO user = new UserDTO("login", "password");
        userRepository.save(user);
        final UserDTO userFromBase = userRepository.findById(user.getId()).orElse(null);
        assertNotNull(userFromBase);
        assertEquals(user.getId(), userFromBase.getId());
    }

    @Test
    public void testFindByLogin() {
        @NotNull final UserDTO user = new UserDTO("login", "password");
        userRepository.save(user);
        final UserDTO userFromBase = userRepository.findByLogin(user.getLogin());
        assertNotNull(userFromBase);
        assertEquals(user.getId(), userFromBase.getId());
    }

    @Test
    public void testUpdate() {
        UserDTO user = new UserDTO("login", "password");
        userRepository.save(user);
        final UserDTO userFromBase = userRepository.findById(user.getId()).orElse(null);
        assertNotNull(userFromBase);
        assertEquals(user.getLogin(), userFromBase.getLogin());

        user.setLogin("login2");
        userRepository.save(user);
        final UserDTO userFromBaseWithAnotherLogin = userRepository.findById(user.getId()).orElse(null);
        assertNotNull(userFromBaseWithAnotherLogin);
        assertEquals(user.getId(), userFromBaseWithAnotherLogin.getId());
        assertEquals(user.getLogin(), userFromBaseWithAnotherLogin.getLogin());
    }

    @Test
    public void testFindAll() {
        userRepository.save(new UserDTO());
        userRepository.save(new UserDTO());
        userRepository.save(new UserDTO());
        assertEquals(3, userRepository.findAll().size());
    }

}
