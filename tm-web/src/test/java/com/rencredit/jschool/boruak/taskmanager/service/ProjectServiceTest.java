package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class ProjectServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    private User admin;

    private User test;

    @Before
    public void setup() throws EmptyLoginException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyPasswordException, EmptyUserException, DeniedAccessException {
        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        SecurityContextHolder.setContext(ctx);
        ctx.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "", Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"))));

        userService.deleteAll();
        projectService.deleteAll();
        admin = userService.findByLoginEntity("admin");
        test = userService.findByLoginEntity("test");
    }

    @Test
    public void testSaveUserIdName() throws EmptyNameException, EmptyUserIdException {
        projectService.save(admin.getId(), "project");
        assertNotNull(projectService.findByNameDTO(admin.getId(), "project"));
    }

    @Test
    public void testSaveUserIdNameDescription() throws EmptyNameException, EmptyUserIdException, EmptyDescriptionException {
        projectService.save(admin.getId(), "project", "description");
        final ProjectDTO project = projectService.findByNameDTO(admin.getId(), "project");
        assertNotNull(project);
        assertEquals("description", project.getDescription());
    }

    @Test
    public void testSaveUserIdProjectDTO() throws EmptyNameException, EmptyUserIdException, EmptyDescriptionException, EmptyProjectException {
        final ProjectDTO project = new ProjectDTO(admin.getId(),"project");
        projectService.save(admin.getId(), project);
        assertNotNull(projectService.findByNameDTO(admin.getId(), "project"));
    }

    @Test
    public void testSaveUserIdProject() throws EmptyNameException, EmptyUserIdException, EmptyDescriptionException, EmptyProjectException {
        final Project project = new Project(admin,"project");
        projectService.save(admin.getId(), project);
        assertNotNull(projectService.findByNameDTO(admin.getId(), "project"));
    }

    @Test
    public void testSaveList() throws EmptyNameException, EmptyUserIdException, EmptyDescriptionException, EmptyProjectListException {
        final List<ProjectDTO> list = new ArrayList<>();
        list.add(new ProjectDTO(admin.getId(),"project1"));
        list.add(new ProjectDTO(admin.getId(),"project2"));

        projectService.saveList(list);
        assertEquals(2, projectService.findAllByUserIdDTO(admin.getId()).size());
    }

    @Test
    public void testFindAllByUserIdDTO() throws EmptyNameException, EmptyUserIdException {
        projectService.save(admin.getId(), "project1");
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(3, projectService.findAllByUserIdDTO(admin.getId()).size());
        assertEquals(1, projectService.findAllByUserIdDTO(test.getId()).size());
    }

    @Test
    public void testFindByIndexDTO() throws EmptyNameException, EmptyUserIdException, IncorrectIndexException {
        projectService.save(admin.getId(), "project");

        assertEquals("project", projectService.findByIndexDTO(admin.getId(), 0).getName());
    }

    @Test
    public void testFindByNameDTO() throws EmptyNameException, EmptyUserIdException {
        projectService.save(admin.getId(), "project");

        assertEquals("project", projectService.findByNameDTO(admin.getId(), "project").getName());
    }

    @Test
    public void testFindByIdDTO() throws EmptyUserIdException, EmptyProjectException, EmptyIdException {
        final ProjectDTO project = new ProjectDTO(admin.getId(),"project");
        projectService.save(admin.getId(), project);
        assertNotNull(projectService.findByIdDTO(admin.getId(), project.getId()));
    }

    @Test
    public void testFindByIdEntity() throws EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyIdException, EmptyProjectIdException {
        final ProjectDTO project = new ProjectDTO(admin.getId(),"project");
        projectService.save(admin.getId(), project);
        final Project projectFromBase = projectService.findByIdEntity(admin.getId(), project.getId());
        assertNotNull(projectFromBase);
    }

    @Test
    public void testFindList() throws EmptyNameException, EmptyUserIdException {
        projectService.save(admin.getId(), "project1");
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(4, projectService.findList().size());
    }

    @Test
    public void testExistsById() throws EmptyUserIdException, EmptyProjectException, EmptyIdException, EmptyProjectIdException {
        final ProjectDTO project = new ProjectDTO(admin.getId(),"project");
        projectService.save(admin.getId(), project);
        assertTrue(projectService.existsById(project.getId()));
        assertFalse(projectService.existsById("34234234"));
    }

    @Test
    public void testExistsByUserIdAndProjectId() throws EmptyUserIdException, EmptyProjectException, EmptyIdException, EmptyProjectIdException {
        final ProjectDTO project = new ProjectDTO(admin.getId(),"project");
        projectService.save(admin.getId(), project);
        assertTrue(projectService.existsByUserIdAndProjectId(admin.getId(), project.getId()));
        assertFalse(projectService.existsByUserIdAndProjectId(admin.getId(),"34234234"));
    }

    @Test
    public void testCount() throws EmptyNameException, EmptyUserIdException {
        projectService.save(admin.getId(), "project1");
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(4, projectService.count());
    }

    @Test
    public void testCountAllByUserId() throws EmptyNameException, EmptyUserIdException {
        projectService.save(admin.getId(), "project1");
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(3, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
    }

    @Test
    public void testDeleteUserIdProject() throws EmptyUserIdException, EmptyProjectException, EmptyIdException {
        final ProjectDTO project = new ProjectDTO(admin.getId(),"project");
        projectService.save(admin.getId(), project);
        assertNotNull(projectService.findByIdDTO(admin.getId(), project.getId()));

        projectService.delete(admin.getId(), project);
        assertNull(projectService.findByIdDTO(admin.getId(), project.getId()));
    }

    @Test
    public void testDeleteAllByUserId() throws EmptyNameException, EmptyUserIdException {
        projectService.save(admin.getId(), "project1");
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(3, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
        projectService.deleteAllByUserId(admin.getId());

        assertEquals(0, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
    }

    @Test
    public void testDeleteAllByIndex() throws EmptyNameException, EmptyUserIdException, EmptyProjectException, IncorrectIndexException {
        projectService.save(admin.getId(), "project1");
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(3, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
        projectService.deleteOneByIndex(admin.getId(), 0);

        assertNull(projectService.findByNameDTO(admin.getId(),"project1"));
        assertEquals(2, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
    }

    @Test
    public void testDeleteOneByName() throws EmptyNameException, EmptyUserIdException, EmptyProjectException, IncorrectIndexException {
        projectService.save(admin.getId(), "project1");
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(3, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
        projectService.deleteOneByName(admin.getId(), "project2");

        assertNull(projectService.findByNameDTO(admin.getId(),"project2"));
        assertEquals(2, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
    }

    @Test
    public void testDeleteOneById() throws EmptyNameException, EmptyUserIdException, EmptyProjectException, IncorrectIndexException, EmptyIdException {
        final ProjectDTO project = new ProjectDTO(admin.getId(), "project1");
        projectService.save(admin.getId(), project);
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(3, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
        projectService.deleteOneById(project.getId());

        assertNull(projectService.findByNameDTO(admin.getId(),"project1"));
        assertEquals(2, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
    }

    @Test
    public void testDeleteByUserIdAndProjectId() throws EmptyNameException, EmptyUserIdException, EmptyProjectException, IncorrectIndexException, EmptyIdException, EmptyProjectIdException {
        final ProjectDTO project = new ProjectDTO(admin.getId(), "project1");
        projectService.save(admin.getId(), project);
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(3, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
        projectService.deleteByUserIdAndProjectId(admin.getId(), project.getId());

        assertNull(projectService.findByNameDTO(admin.getId(),"project1"));
        assertEquals(2, projectService.countAllByUserId(admin.getId()));
        assertEquals(1, projectService.countAllByUserId(test.getId()));
    }

    @Test
    public void testDeleteAll() throws EmptyNameException, EmptyUserIdException, EmptyProjectException, IncorrectIndexException, EmptyIdException, EmptyProjectIdException {
        projectService.save(admin.getId(), "project1");
        projectService.save(admin.getId(), "project2");
        projectService.save(admin.getId(), "project3");
        projectService.save(test.getId(), "project4");

        assertEquals(4, projectService.count());
        projectService.deleteAll();
        assertEquals(0, projectService.count());
    }

    @Test
    public void testDeleteList() throws EmptyNameException, EmptyUserIdException, EmptyProjectException, IncorrectIndexException, EmptyIdException, EmptyProjectIdException, EmptyProjectListException {
        final List<ProjectDTO> list1 = new ArrayList<>();
        final ProjectDTO project1 = new ProjectDTO(admin.getId(),"project1");
        final ProjectDTO project2 = new ProjectDTO(admin.getId(),"project2");
        final ProjectDTO project3 = new ProjectDTO(admin.getId(),"project3");
        list1.add(project1);
        list1.add(project2);
        list1.add(project3);
        projectService.saveList(list1);
        assertEquals(3, projectService.count());

        final List<ProjectDTO> list2 = new ArrayList<>();
        list2.add(project2);
        list2.add(project3);
        projectService.deleteList(list2);
        assertEquals(1, projectService.count());
    }

}
