package com.rencredit.jschool.boruak.taskmanager.repository.entity;

import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IUserRepositoryEntity;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class UserRepositoryEntityTest {

    @Autowired
    private IUserRepositoryEntity userRepository;

    @Before
    public void setup() {
        userRepository.deleteAll();
    }

    @Test
    public void testSave() {
        final User user = new User("login", "password");
        userRepository.save(user);
        assertNotNull(userRepository.findById(user.getId()));
    }

    @Test
    public void testFindById() {
        final User user = new User("login", "password");
        userRepository.save(user);
        final User userFromBase = userRepository.findById(user.getId()).orElse(null);
        assertNotNull(userFromBase);
        assertEquals(user.getId(), userFromBase.getId());
    }

    @Test
    public void testFindByLogin() {
        @NotNull final User user = new User("login", "password");
        userRepository.save(user);
        final User userFromBase = userRepository.findByLogin(user.getLogin());
        assertNotNull(userFromBase);
        assertEquals(user.getId(), userFromBase.getId());
    }

    @Test
    public void testGetOne() {
        final User user = new User("login", "password");
        userRepository.save(user);
        final User userFromBase = userRepository.getOne(user.getId());
        assertNotNull(userFromBase);
        assertEquals(user.getId(), userFromBase.getId());
    }

    @Test
    public void testDeleteById() {
        final User user = new User("login", "password");
        userRepository.save(user);
        final User userFromBase = userRepository.findById(user.getId()).orElse(null);
        assertNotNull(userFromBase);
        userRepository.deleteById(user.getId());
        final User userFromBaseAfterDelete = userRepository.findById(user.getId()).orElse(null);
        assertNull(userFromBaseAfterDelete);
    }

    @Test
    @Transactional
    public void testDeleteByLogin() {
        @NotNull final User user = new User("login", "password");
        userRepository.save(user);
        final User userFromBase = userRepository.findById(user.getId()).orElse(null);
        assertNotNull(userFromBase);
        String login = user.getLogin();
        userRepository.deleteByLogin(login);
        final User userFromBaseAfterDelete = userRepository.findById(user.getId()).orElse(null);
        assertNull(userFromBaseAfterDelete);
    }

    @Test
    public void testUpdate() {
        User user = new User("login", "password");
        userRepository.save(user);
        final User userFromBase = userRepository.findById(user.getId()).orElse(null);
        assertNotNull(userFromBase);
        assertEquals(user.getLogin(), userFromBase.getLogin());

        user.setLogin("login2");
        userRepository.save(user);
        final User userFromBaseWithAnotherLogin = userRepository.findById(user.getId()).orElse(null);
        assertNotNull(userFromBaseWithAnotherLogin);
        assertEquals(user.getId(), userFromBaseWithAnotherLogin.getId());
        assertEquals(user.getLogin(), userFromBaseWithAnotherLogin.getLogin());
    }

    @Test
    public void testFindAll() {
        userRepository.save(new User());
        userRepository.save(new User());
        userRepository.save(new User());
        assertEquals(3, userRepository.findAll().size());
    }

    @Test
    public void testDeleteAll() {
        userRepository.save(new User());
        userRepository.save(new User());
        userRepository.save(new User());
        assertEquals(3, userRepository.findAll().size());
        userRepository.deleteAll();
        assertEquals(0, userRepository.findAll().size());
    }

}
