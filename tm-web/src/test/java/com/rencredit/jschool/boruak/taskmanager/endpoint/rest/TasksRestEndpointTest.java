package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class TasksRestEndpointTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserService userService;

    @Autowired
    private TaskService taskService;

    private MockMvc mockMvc;

    private User admin;

    private User test;

    @Before
    public void setup() throws EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyPasswordException, EmptyRoleException, EmptyHashLineException {
        this.mockMvc = MockMvcBuilders
                        .webAppContextSetup(this.webApplicationContext)
                        .build();

        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        SecurityContextHolder.setContext(ctx);
        ctx.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "", Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"))));
        userService.deleteAll();
        admin = userService.findByLoginEntity("admin");
        test = userService.findByLoginEntity("test");

        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    public void testGetListDTO() throws Exception {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/tasks")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..name", hasItem("task1")))
                .andExpect(jsonPath("$..name", hasItem("task2")));
    }

    @Test
    public void testGetListDTOAll() throws Exception {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(test.getId(), "task3");
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/tasks/all")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..name", hasItem("task1")))
                .andExpect(jsonPath("$..name", hasItem("task2")))
                .andExpect(jsonPath("$..name", hasItem("task3")));
    }

    @Test
    public void testSaveAll() throws Exception {
        final List<TaskDTO> list = new ArrayList<>();
        list.add(new TaskDTO(admin.getId(),"task1"));
        list.add(new TaskDTO(admin.getId(),"task2"));

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(list);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/tasks")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        assertNotNull(taskService.findOneByNameDTO(admin.getId(), "task1"));
        assertNotNull(taskService.findOneByNameDTO(admin.getId(), "task2"));
    }

    @Test
    public void testCount() throws Exception {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(test.getId(), "task3");
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/tasks/count")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("2"));
    }

    @Test
    public void testDeleteAll() throws Exception {
        taskService.save(admin.getId(), "task1");
        taskService.save(admin.getId(), "task2");
        taskService.save(test.getId(), "task3");
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/tasks/all")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        assertEquals(1, taskService.findListDTO().size());
    }

}
