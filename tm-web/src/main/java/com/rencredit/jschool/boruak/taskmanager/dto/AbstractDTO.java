package com.rencredit.jschool.boruak.taskmanager.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
public class AbstractDTO implements Serializable {

    public static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    private String id = UUID.randomUUID().toString();

}
