package com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.jetbrains.annotations.Nullable;

import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/task")
public interface ITaskRestEndpoint {

//    static ITaskRestEndpoint client(final String baseUrl) {
//        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
//        final HttpMessageConverters converters = new HttpMessageConverters(converter);
//        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
//        return Feign.builder()
//                .contract(new SpringMvcContract())
//                .encoder(new SpringEncoder(objectFactory))
//                .decoder(new SpringDecoder(objectFactory))
//                .target(ITaskRestEndpoint.class, baseUrl);
//    }

    @PostMapping
    void create(@RequestBody TaskDTO task) throws EmptyTaskException, EmptyUserIdException, DeniedAccessException, UnknownUserException;

    @Nullable
    @GetMapping("/${id}")
    TaskDTO findOneByIdDTO(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException;

    @GetMapping("/exist/${id}")
    boolean existsById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyTaskIdException;

    @DeleteMapping("/${id}")
    void deleteOneById(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException;

}
