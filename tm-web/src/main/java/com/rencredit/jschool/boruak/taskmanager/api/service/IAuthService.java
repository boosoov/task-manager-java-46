package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    boolean checkRoles(@Nullable final String userId, @Nullable final Role[] roles) throws DeniedAccessException, EmptyUserIdException, EmptyRoleException, NotExistUserException, EmptyIdException;

    void registration(@Nullable final String login, @Nullable final String password) throws EmptyPasswordException, EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyHashLineException;

    void registration(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws EmptyRoleException, EmptyPasswordException, EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyHashLineException;

}
