package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.soap.IAdminUserEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/services/AdminUserEndpoint?wsdl
 */

@WebService
@Component
public class AdminUserEndpoint implements IAdminUserEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private IAuthService authService;

    public AdminUserEndpoint() {
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO lockUserByLogin(
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws DeniedAccessException, EmptyLoginException, EmptyUserException {
        return userService.lockUserByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO unlockUserByLogin(
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws DeniedAccessException, EmptyLoginException, EmptyUserException {
        return userService.unlockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void addUserLoginPasswordRole(
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) throws EmptyRoleException, DeniedAccessException, EmptyUserException, EmptyHashLineException, EmptyLoginException, EmptyPasswordException, BusyLoginException {
        userService.save(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO getUserById(
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException {
        return userService.findByIdDTO(id);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO getUserByLogin(
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException {
        return userService.findByLoginDTO(login);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException {
        userService.deleteById(id);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException {
        userService.deleteByLogin(login);
    }

    @Override
    @WebMethod
    public void clearAllUser(
    ) throws DeniedAccessException, EmptyRoleException, EmptyUserException, EmptyLoginException, EmptyPasswordException, BusyLoginException, EmptyHashLineException, UnknownUserException {
        userService.deleteAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<UserDTO> getUserList() {
        return userService.findListDTO();
    }

}
