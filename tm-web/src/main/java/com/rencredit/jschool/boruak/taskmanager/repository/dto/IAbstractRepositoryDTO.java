package com.rencredit.jschool.boruak.taskmanager.repository.dto;

import com.rencredit.jschool.boruak.taskmanager.dto.AbstractDTO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAbstractRepositoryDTO<E extends AbstractDTO> extends JpaRepository<E, String> {

}
