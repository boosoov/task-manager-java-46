package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IDataBasePropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService, IDataBasePropertyService {

    @Autowired
    private Environment env;

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getServerHost() {
        return env.getProperty("server.host");
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Integer getServerPort() {
        return Integer.parseInt(env.getProperty("server.port"));
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getJdbcDriver() {
        return env.getProperty("jdbcDriver");
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getJdbcUrl() {
        return env.getProperty("db.host");
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getJdbcUsername() {
        return env.getProperty("db.login");
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String getJdbcPassword() {
        return env.getProperty("db.password");
    }
}
