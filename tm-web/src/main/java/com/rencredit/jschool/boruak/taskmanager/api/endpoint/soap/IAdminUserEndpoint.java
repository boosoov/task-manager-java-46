package com.rencredit.jschool.boruak.taskmanager.api.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @Nullable
    @WebMethod
    UserDTO lockUserByLogin(
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws DeniedAccessException, EmptyLoginException, EmptyUserException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

    @Nullable
    @WebMethod
    UserDTO unlockUserByLogin(
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws DeniedAccessException, EmptyLoginException, EmptyUserException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

    @WebMethod
    void addUserLoginPasswordRole(
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) throws EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptySessionException, UnknownUserException;

    @Nullable
    @WebMethod
    UserDTO getUserById(
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, DeniedAccessException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

    @Nullable
    @WebMethod
    UserDTO getUserByLogin(
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

    @WebMethod
    void removeUserById(
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, DeniedAccessException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

    @WebMethod
    void removeUserByLogin(
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

    @WebMethod
    void clearAllUser() throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyUserException, EmptyLoginException, EmptyPasswordException, BusyLoginException, EmptyHashLineException, UnknownUserException;

    @NotNull
    @WebMethod
    List<UserDTO> getUserList() throws DeniedAccessException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

}
