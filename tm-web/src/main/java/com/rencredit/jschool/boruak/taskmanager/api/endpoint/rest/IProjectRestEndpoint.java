package com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/project")
public interface IProjectRestEndpoint {

//    static IProjectRestEndpoint client(final String baseUrl) {
//        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
//        final HttpMessageConverters converters = new HttpMessageConverters(converter);
//        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
//        return Feign.builder()
//                .contract(new SpringMvcContract())
//                .encoder(new SpringEncoder(objectFactory))
//                .decoder(new SpringDecoder(objectFactory))
//                .target(IProjectRestEndpoint.class, baseUrl);
//    }

    @PostMapping
    void create(@RequestBody ProjectDTO project) throws EmptyUserIdException, EmptyProjectException, EmptyLoginException, DeniedAccessException, UnknownUserException;

    @Nullable
    @GetMapping("/${id}")
    ProjectDTO findOneByIdDTO(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException;

    @GetMapping("/exist/{id}")
    boolean existsById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyProjectIdException;

    @DeleteMapping("/${id}")
    void deleteOneById(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException, EmptyProjectIdException;

}
