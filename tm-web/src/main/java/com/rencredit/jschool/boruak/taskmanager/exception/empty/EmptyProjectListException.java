package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyProjectListException extends AbstractException {

    public EmptyProjectListException() {
        super("Error! Project list not exist...");
    }

}
