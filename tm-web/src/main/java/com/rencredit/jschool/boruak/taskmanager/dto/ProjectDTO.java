package com.rencredit.jschool.boruak.taskmanager.dto;

import com.rencredit.jschool.boruak.taskmanager.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "app_project")
public class ProjectDTO extends AbstractDTO implements Serializable {

    public static final long serialVersionUID = 1L;

    @Nullable
    @Column(name = "name")
    private String name;

    @Nullable
    @Column(name = "description")
    private String description = "";

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column(name = "date_start")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Column(name = "date_finish")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    @Column(name = "user_Id")
    private String userId;

    public ProjectDTO() {
    }

    public ProjectDTO(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        this.userId = userId;
        this.name = name;
    }

    public ProjectDTO(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return "Project{" +
                "id='" + super.getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }

}
