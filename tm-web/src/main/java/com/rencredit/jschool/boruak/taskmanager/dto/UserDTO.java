package com.rencredit.jschool.boruak.taskmanager.dto;

import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "app_user")
public class UserDTO extends AbstractDTO implements Serializable {

    public static final long serialVersionUID = 1L;

    @Nullable
    @Column(name = "login")
    private String login;

    @Nullable
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "role")
    private Role role = Role.USER;

    @Column(name = "locked")
    private boolean locked = false;

    public UserDTO() {
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @NotNull final String firstName) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.firstName = firstName;
    }

    @NotNull
    @Override
    public String toString() {
        return "User{" +
                "id='" + super.getId() + '\'' +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", role=" + role +
                ", locked=" + locked +
                '}';
    }

}
