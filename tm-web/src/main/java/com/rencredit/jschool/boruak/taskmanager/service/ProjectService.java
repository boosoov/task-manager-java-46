package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IProjectService;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.IProjectRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IProjectRepositoryEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepositoryDTO repositoryDTO;

    @Autowired
    private IProjectRepositoryEntity repositoryEntity;

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void save(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDTO project = new ProjectDTO(userId, name);
        repositoryDTO.save(project);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void save(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDTO project = new ProjectDTO(userId, name, description);
        repositoryDTO.save(project);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void save(@Nullable final String userId, @Nullable final ProjectDTO project) throws EmptyProjectException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyProjectException();
        project.setUserId(userId);
        repositoryDTO.save(project);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void save(@Nullable final String userId, @Nullable final Project project) throws EmptyProjectException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyProjectException();
        repositoryEntity.save(project);
    }

    @Transactional
    public List<ProjectDTO> saveList(@Nullable final Iterable<ProjectDTO> iterable) throws EmptyProjectListException {
        if (iterable == null) throw new EmptyProjectListException();
        return repositoryDTO.saveAll(iterable);
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List<ProjectDTO> findAllByUserIdDTO(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @NotNull final List<ProjectDTO> projects = repositoryDTO.findAllByUserId(userId);
        return projects;
    }

    @Nullable
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ProjectDTO findByIndexDTO(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        @NotNull final ProjectDTO project = findAllByUserIdDTO(userId).get(index);
        return project;
    }

    @Nullable
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ProjectDTO findByNameDTO(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final ProjectDTO project = repositoryDTO.findByUserIdAndName(userId, name);
        return project;
    }

    @Nullable
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public ProjectDTO findByIdDTO(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        return repositoryDTO.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public Project findByIdEntity(@Nullable final String userId, @Nullable final String projectId) throws EmptyIdException, EmptyUserIdException, EmptyProjectIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @Nullable final Optional<Project> opt = repositoryEntity.findById(projectId);
        return opt.isPresent() ? opt.get() : null;
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<ProjectDTO> findList() {
        @NotNull final List<ProjectDTO> projects = repositoryDTO.findAll();
        return projects;
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public boolean existsById(@Nullable final String projectId) throws EmptyProjectIdException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        return repositoryDTO.existsById(projectId);
    }

    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public boolean existsByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws EmptyUserIdException, EmptyProjectIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        return repositoryEntity.existsByUserIdAndId(userId, projectId);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public long count() {
        return repositoryDTO.count();
    }

    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public long countAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        return repositoryDTO.countAllByUserId(userId);
    }


    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void delete(@Nullable final String userId, @Nullable final ProjectDTO project) throws EmptyProjectException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new EmptyProjectException();

        repositoryEntity.deleteById(project.getId());
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        repositoryEntity.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException, EmptyProjectException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        @Nullable final ProjectDTO project = findByIndexDTO(userId, index);
        if (project == null) throw new EmptyProjectException();
        @NotNull final String projectId = project.getId();
        repositoryEntity.deleteById(projectId);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        repositoryEntity.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteOneById(@Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        repositoryEntity.deleteById(id);
    }

    @Override
    @Nullable
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public Integer deleteByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws EmptyUserIdException, EmptyProjectIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        return repositoryEntity.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteAll() {
        repositoryEntity.deleteAll();
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteList(@Nullable final Iterable<ProjectDTO> iterable) throws EmptyProjectListException {
        if (iterable == null) throw new EmptyProjectListException();

        repositoryDTO.deleteAll(iterable);
    }

}
