package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IProjectService {

    void save(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException;

    void save(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException;

    void save(@Nullable final String userId, @Nullable final ProjectDTO project) throws EmptyProjectException, EmptyUserIdException;

    void save(@Nullable final String userId, @Nullable final Project project) throws EmptyProjectException, EmptyUserIdException;

    List<ProjectDTO> saveList(@Nullable final Iterable<ProjectDTO> iterable) throws EmptyProjectListException;


    @Nullable
    ProjectDTO findByIdDTO(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    Project findByIdEntity(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException, EmptyProjectIdException;

    @Nullable
    ProjectDTO findByIndexDTO(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException;

    @Nullable
    ProjectDTO findByNameDTO(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException;

    @NotNull
    List<ProjectDTO> findAllByUserIdDTO(@Nullable final String userId) throws EmptyUserIdException;

    @NotNull List<ProjectDTO> findList();


    boolean existsById(@Nullable final String projectId) throws EmptyProjectIdException;

    boolean existsByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) throws EmptyUserIdException, EmptyProjectIdException;


    long count();

    long countAllByUserId(@NotNull final String userId) throws EmptyUserIdException;


    @Nullable
    void deleteOneById(@Nullable final String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    Integer deleteByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws EmptyUserIdException, EmptyProjectIdException;

    @Nullable
    void deleteOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException, EmptyProjectException;

    @Nullable
    void deleteOneByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException;

    void delete(@Nullable final String userId, @Nullable final ProjectDTO project) throws EmptyProjectException, EmptyUserIdException;

    void deleteAllByUserId(@Nullable final String userId) throws EmptyUserIdException;

    void deleteList(@Nullable final Iterable<ProjectDTO> iterable) throws EmptyProjectListException;

    void deleteAll();

}
