package com.rencredit.jschool.boruak.taskmanager.api.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    UserDTO updateUserPassword(
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") String newPassword
    ) throws EmptyUserException, IncorrectHashPasswordException, EmptyIdException, EmptyNewPasswordException, EmptyHashLineException, DeniedAccessException, UnknownUserException, NotExistUserException, EmptyRoleException, EmptyUserIdException;

    @WebMethod
    void updateUser(
            @Nullable @WebParam(name = "user", partName = "user") UserDTO user
    ) throws EmptyUserException, EmptyIdException, DeniedAccessException, UnknownUserException, NotExistUserException, EmptyLoginException, BusyLoginException;

    @Nullable
    @WebMethod
    UserDTO getMyUser() throws DeniedAccessException, UnknownUserException, EmptyIdException;

}