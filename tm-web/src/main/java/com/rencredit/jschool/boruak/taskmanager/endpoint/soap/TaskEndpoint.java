package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/services/TaskEndpoint?wsdl
 */

@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private TaskService taskService;

    @WebMethod
    public void createTask(
            @WebParam(name = "task") TaskDTO task
    ) throws EmptyTaskException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        task.setUserId(UserUtil.getUserId());
        taskService.save(task);
    }

    @WebMethod
    public TaskDTO findTaskByIdDTO(
            @WebParam(name = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return taskService.findOneByIdDTO(UserUtil.getUserId(), id);
    }

    @WebMethod
    public void deleteTaskById(
            @WebParam(name = "id") String id
    ) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyIdException {
        taskService.deleteById(UserUtil.getUserId(), id);
    }

    @WebMethod
    public List<TaskDTO> getListTasks() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return taskService.findAllByUserIdDTO(UserUtil.getUserId());
    }

    @WebMethod
    public List<TaskDTO> getListAllUsersTasks() {
        return taskService.findListDTO();
    }

    @WebMethod
    public void deleteAllTasks() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        taskService.deleteAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public boolean existsTaskById(
            @WebParam(name = "id") String id
    ) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyTaskIdException {
        return taskService.existsByUserIdAndTaskId(UserUtil.getUserId(), id);
    }

}
