package com.rencredit.jschool.boruak.taskmanager.repository.entity;

import com.rencredit.jschool.boruak.taskmanager.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface IUserRepositoryEntity extends IAbstractRepositoryEntity<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    void deleteByLogin(@NotNull String login);

}
