package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.dto.CustomUser;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Status;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal CustomUser user
            ) throws EmptyNameException, EmptyUserIdException {
        projectService.save(user.getUserId(), "Empty");
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyIdException, EmptyUserIdException {
        projectService.deleteOneById(id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyIdException, EmptyUserIdException, EmptyProjectIdException {
        final Project project = projectService.findByIdEntity(user.getUserId(), id);
        return new ModelAndView("project/project-edit", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @ModelAttribute("project") Project project,
            BindingResult result,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyProjectException, EmptyUserIdException, EmptyIdException {
        @NotNull final User userById = userService.findByIdEntity(user.getUserId());
        project.setUser(userById);
        projectService.save(user.getUserId(), project);
        return "redirect:/projects";
    }

}
