package com.rencredit.jschool.boruak.taskmanager.api.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Result;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthEndpoint {

    @WebMethod
    Result login(
            @WebParam(name = "username") final String username,
            @WebParam(name = "password") final String password
    ) throws EmptyLoginException, UnknownUserException, DeniedAccessException;

    @WebMethod
    Result logout();

    @WebMethod
    UserDTO profile() throws DeniedAccessException, UnknownUserException, EmptyIdException;

    @WebMethod
    void registrationLoginPassword(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException;

}
