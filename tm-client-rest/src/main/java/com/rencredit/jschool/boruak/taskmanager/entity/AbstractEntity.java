package com.rencredit.jschool.boruak.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Setter
@Getter
public class AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String id = UUID.randomUUID().toString();

}
