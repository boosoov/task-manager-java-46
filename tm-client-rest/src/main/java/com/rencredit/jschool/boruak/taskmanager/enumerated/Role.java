package com.rencredit.jschool.boruak.taskmanager.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("User"),
    ADMIN("Admin");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
