package com.rencredit.jschool.boruak.taskmanager.dto;

import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Setter
@Getter
public class UserDTO extends AbstractDTO implements Serializable {

    public static final long serialVersionUID = 1L;

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    private boolean locked = false;

    public UserDTO() {
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public UserDTO(@NotNull final String login, @NotNull final String passwordHash, @NotNull final String firstName) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.firstName = firstName;
    }

    @NotNull
    @Override
    public String toString() {
        return "User{" +
                "id='" + super.getId() + '\'' +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", role=" + role +
                ", locked=" + locked +
                '}';
    }

}
