package com.rencredit.jschool.boruak.taskmanager;

import com.rencredit.jschool.boruak.taskmanager.bootstrap.Bootstrap;
import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Client {

    public static void main(String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
