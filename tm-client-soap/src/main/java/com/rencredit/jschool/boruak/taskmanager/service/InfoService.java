package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IInfoService;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.NumberUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class InfoService implements IInfoService {

    @Autowired
    private CommandService commandService;

    @NotNull
    @Override
    public String getArgumentList() {
        @NotNull final String[] arguments = commandService.getArgs();
        @NotNull final StringBuilder resultString = new StringBuilder();
        for (@NotNull final String argument : arguments) {
            resultString.append(argument).append("\n");
        }
        return resultString.toString();
    }

    @NotNull
    @Override
    public String getCommandList() {
        @NotNull final String[] commands = commandService.getCommands();
        @NotNull final StringBuilder resultString = new StringBuilder();
        for (@NotNull final String command : commands) {
            resultString.append(command).append("\n");
        }
        return resultString.toString();

    }

    @NotNull
    @Override
    public String getDeveloperInfo() {
        return "NAME: Boruak Sergey\n" +
                "E-MAIL: boosoov@gmail.com\n";
    }

    @NotNull
    @Override
    public String getHelp() throws EmptyCommandException {
        @NotNull final Map<String, AbstractListener> commands = commandService.getTerminalCommands();
        @NotNull final StringBuilder resultString = new StringBuilder();
        for (@NotNull final Map.Entry<String, AbstractListener> command : commands.entrySet()) {
            @NotNull final AbstractListener commandValue = command.getValue();
            if (commandValue == null) throw new EmptyCommandException();
            if (!commandValue.name().isEmpty()) resultString.append(commandValue.name());
            @Nullable final String arg = commandValue.arg();
            if (arg != null && !arg.isEmpty()) resultString.append(", ").append(commandValue.arg());
            if (!commandValue.description().isEmpty()) resultString.append(": ").append(commandValue.description());
            resultString.append("\n");
        }
        return resultString.toString();
    }

    @NotNull
    @Override
    public String getSystemInfo() {
        @NotNull final StringBuilder resultString = new StringBuilder();
        @NotNull final int processors = Runtime.getRuntime().availableProcessors();
        resultString.append("Available processors: ").append(processors).append("\n");
        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
        resultString.append("Free memory: ").append(NumberUtil.formatBytes(freeMemory)).append("\n");
        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        resultString.append("Maximum memory: ").append(maxMemoryValue).append("\n");
        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
        resultString.append("Total memory available to JVM: ").append(NumberUtil.formatBytes(totalMemory)).append("\n");
        @NotNull final long usedMemory = totalMemory - freeMemory;
        resultString.append("Used memory by JVM: ").append(NumberUtil.formatBytes(usedMemory)).append("\n");
        return resultString.toString();
    }

    @NotNull
    @Override
    public String getVersion() {
        return "1.0.0\n";
    }

}
