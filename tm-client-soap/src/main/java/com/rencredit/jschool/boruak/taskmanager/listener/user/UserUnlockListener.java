package com.rencredit.jschool.boruak.taskmanager.listener.user;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserUnlockListener extends AbstractListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "unlock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user";
    }

    @Override
    @EventListener(condition = "@userUnlockListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyLoginException_Exception {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();

        adminUserEndpoint.unlockUserByLogin(login);
        System.out.println("OK");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
