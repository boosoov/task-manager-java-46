package com.rencredit.jschool.boruak.taskmanager.listener.project;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProjectListAllUsersShowListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list-all";
    }

    @NotNull
    @Override
    public String description() {
        return "Show projects list all users.";
    }

    @Override
    @EventListener(condition = "@projectListAllUsersShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws EmptyUserException, EmptyUserIdException_Exception, UnknownUserException_Exception, DeniedAccessException_Exception {
        System.out.println("[LIST PROJECTS ALL USERS]");

        @NotNull final List<ProjectDTO> projects = projectEndpoint.getListAllUsersProjects();
        @NotNull int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ". ");
            ViewUtil.showProject(project);
            index++;
        }
        System.out.println("[OK]");
    }

}
