package com.rencredit.jschool.boruak.taskmanager.listener.info;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.AdminEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.EmptyRoleException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.NotExistUserException_Exception;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ServerInfoShowListener extends AbstractListener {

    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String description() {
        return "Show show host and port program.";
    }

    @Override
    @EventListener(condition = "@serverInfoShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[SERVER INFO]");
        System.out.println(adminEndpoint.getHostPort());
        System.out.println("OK");
    }

}
