
package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-03-04T18:07:38.999+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "EmptyTaskException", targetNamespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/")
public class EmptyTaskException_Exception extends Exception {

    private com.rencredit.jschool.boruak.taskmanager.endpoint.soap.EmptyTaskException emptyTaskException;

    public EmptyTaskException_Exception() {
        super();
    }

    public EmptyTaskException_Exception(String message) {
        super(message);
    }

    public EmptyTaskException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public EmptyTaskException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.soap.EmptyTaskException emptyTaskException) {
        super(message);
        this.emptyTaskException = emptyTaskException;
    }

    public EmptyTaskException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.soap.EmptyTaskException emptyTaskException, java.lang.Throwable cause) {
        super(message, cause);
        this.emptyTaskException = emptyTaskException;
    }

    public com.rencredit.jschool.boruak.taskmanager.endpoint.soap.EmptyTaskException getFaultInfo() {
        return this.emptyTaskException;
    }
}
