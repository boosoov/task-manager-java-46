package com.rencredit.jschool.boruak.taskmanager.listener.user;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ProfileShowListener extends AbstractListener {

    @Autowired
    private AuthEndpoint authEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "view-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Show profile.";
    }

    @Override
    @EventListener(condition = "@profileShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws DeniedAccessException_Exception, UnknownUserException_Exception, EmptyIdException_Exception, EmptyUserException {
        final UserDTO user = authEndpoint.profile();
        ViewUtil.showUser(user);
    }

}
