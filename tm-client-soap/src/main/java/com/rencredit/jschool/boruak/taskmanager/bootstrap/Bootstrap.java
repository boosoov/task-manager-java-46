package com.rencredit.jschool.boruak.taskmanager.bootstrap;

import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalCommandUtil;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap {

    @Autowired
    private ApplicationEventPublisher publisher;

    public Bootstrap() {
    }

    public void run(@Nullable final String[] args) {
        @NotNull String[] commandsFromUser = null;
        System.out.println("** WELCOME TO TASK MANAGER ** \n");
        parseArgs(args);
        while (true) {
            try {
                commandsFromUser = TerminalUtil.nextLine().split("\\s+");
                parseArgs(commandsFromUser);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        for (@Nullable final String arg : args) {
            processArg(TerminalCommandUtil.convertArgumentToCommand(arg));
            System.out.println();
        }
    }

    @SneakyThrows
    private void processArg(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        publisher.publishEvent(new ConsoleEvent(command));
    }

}
