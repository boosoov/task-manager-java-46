package com.rencredit.jschool.boruak.taskmanager.listener.user.auth;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import com.rencredit.jschool.boruak.taskmanager.service.SessionService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class UserLogOutListener extends AbstractListener {

//    @Autowired
//    private AuthEndpoint authEndpoint;
//
//    @Autowired
//    private ProjectEndpoint projectEndpoint;
//
//    @Autowired
//    private TaskEndpoint taskEndpoint;
//
//    @Autowired
//    private UserEndpoint userEndpoint;
//
//    @Autowired
//    private AdminEndpoint adminEndpoint;
//
//    @Autowired
//    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private AuthService authService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout system.";
    }

    @Override
    @EventListener(condition = "@userLogOutListener.name() == #event.command")
    public void handle(final ConsoleEvent event) {
//        SessionService.setListCookieRowRequest(authEndpoint, new ArrayList<>());
//        SessionService.setListCookieRowRequest(projectEndpoint, new ArrayList<>());
//        SessionService.setListCookieRowRequest(taskEndpoint, new ArrayList<>());
//        SessionService.setListCookieRowRequest(userEndpoint, new ArrayList<>());
//        SessionService.setListCookieRowRequest(adminEndpoint, new ArrayList<>());
//        SessionService.setListCookieRowRequest(adminUserEndpoint, new ArrayList<>());
//        authEndpoint.logout();
        authService.logout();
        System.out.println("Have been logout");
    }

}
