package com.rencredit.jschool.boruak.taskmanager.constant;

import org.jetbrains.annotations.NotNull;

public interface TerminalConst {

    @NotNull String HELP = "help";

    @NotNull String VERSION = "version";

    @NotNull String ABOUT = "about";

    @NotNull String EXIT = "exit";

    @NotNull String INFO = "info";

    @NotNull String COMMANDS = "commands";

    @NotNull String ARGUMENTS = "arguments";

    @NotNull String TASK_CREATE = "task-create";

    @NotNull String TASK_CLEAR = "task-clear";

    @NotNull String TASK_LIST = "task-list";

    @NotNull String PROJECT_CREATE = "project-create";

    @NotNull String PROJECT_CLEAR = "project-clear";

    @NotNull String PROJECT_LIST = "project-list";

    @NotNull String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    @NotNull String TASK_UPDATE_BY_ID = "task-update-by-id";

    @NotNull String TASK_VIEW_BY_ID = "task-view-by-id";

    @NotNull String TASK_VIEW_BY_NAME = "task-view-by-name";

    @NotNull String TASK_VIEW_BY_INDEX = "task-view-by-index";

    @NotNull String TASK_REMOVE_BY_ID = "task-remove-by-id";

    @NotNull String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    @NotNull String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    @NotNull String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    @NotNull String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    @NotNull String PROJECT_VIEW_BY_ID = "project-view-by-id";

    @NotNull String PROJECT_VIEW_BY_NAME = "project-view-by-name";

    @NotNull String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    @NotNull String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    @NotNull String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    @NotNull String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    @NotNull String REGISTRATION = "registration";

    @NotNull String LOG_IN = "login";

    @NotNull String LOG_OUT = "logout";

    @NotNull String UPDATE_PASSWORD = "update-password";

    @NotNull String VIEW_PROFILE = "view-profile";

    @NotNull String EDIT_PROFILE = "edit-profile";

}
