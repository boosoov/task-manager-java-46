package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyIdException extends AbstractClientException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}
