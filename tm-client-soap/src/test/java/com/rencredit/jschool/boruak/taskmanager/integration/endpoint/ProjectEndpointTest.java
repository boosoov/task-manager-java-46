package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class ProjectEndpointTest {

    @Autowired
    private UserEndpoint userEndpoint;

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private AuthService authService;

    @Autowired
    private ProjectEndpoint projectEndpoint;

    private UserDTO admin;

    @Before
    public void init() throws EmptyLoginException_Exception, DeniedAccessException_Exception, UnknownUserException_Exception, EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        authService.login("admin", "admin");
        adminUserEndpoint.clearAllUser();
        authService.login("admin", "admin");
        projectEndpoint.deleteAllProjects();
        admin = userEndpoint.getMyUser();
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, UnknownUserException_Exception, EmptyUserIdException_Exception {
        authService.login("admin", "admin");
        projectEndpoint.deleteAllProjects();
        adminUserEndpoint.clearAllUser();
    }

    @Test
    public void testCreateProject() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyProjectException_Exception {
        final ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(admin.getId());
        project.setName("project");
        projectEndpoint.createProject(project);
        final ProjectDTO projectFromServer = projectEndpoint.findProjectByIdDTO(project.getId());
        assertEquals(admin.getId(), projectFromServer.getUserId());
        assertEquals("project", projectFromServer.getName());
    }

    @Test
    public void testFindProjectByIdDTO() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyProjectException_Exception {
        final ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(admin.getId());
        projectEndpoint.createProject(project);
        assertNotNull(projectEndpoint.findProjectByIdDTO(project.getId()));
    }

    @Test
    public void testDeleteProject() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyProjectException_Exception {
        final ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(admin.getId());
        projectEndpoint.createProject(project);
        assertNotNull(projectEndpoint.findProjectByIdDTO(project.getId()));
        projectEndpoint.deleteProject(project);
        assertNull(projectEndpoint.findProjectByIdDTO(project.getId()));
    }

    @Test
    public void testDeleteProjectById() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyProjectException_Exception, EmptyProjectIdException_Exception {
        final ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(admin.getId());
        projectEndpoint.createProject(project);
        assertNotNull(projectEndpoint.findProjectByIdDTO(project.getId()));
        projectEndpoint.deleteProjectById(project.getId());
        assertNull(projectEndpoint.findProjectByIdDTO(project.getId()));
    }

    @Test
    public void testGetListProjects() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyProjectException_Exception, EmptyProjectIdException_Exception {
        final ProjectDTO project1 = new ProjectDTO();
        project1.setId(UUID.randomUUID().toString());
        project1.setUserId(admin.getId());
        projectEndpoint.createProject(project1);
        final ProjectDTO project2 = new ProjectDTO();
        project2.setId(UUID.randomUUID().toString());
        project2.setUserId(admin.getId());
        projectEndpoint.createProject(project2);

        assertEquals(2, projectEndpoint.getListProjects().size());
    }

    @Test
    public void testGetListAllUsersProjects() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyProjectException_Exception, EmptyProjectIdException_Exception {
        final ProjectDTO project1 = new ProjectDTO();
        project1.setId(UUID.randomUUID().toString());
        project1.setUserId(admin.getId());
        projectEndpoint.createProject(project1);
        final ProjectDTO project2 = new ProjectDTO();
        project2.setId(UUID.randomUUID().toString());
        project2.setUserId(admin.getId());
        projectEndpoint.createProject(project2);

        authService.login("test", "test");
        admin = userEndpoint.getMyUser();
        final ProjectDTO project3 = new ProjectDTO();
        project3.setId(UUID.randomUUID().toString());
        project3.setUserId(admin.getId());
        projectEndpoint.createProject(project3);

        authService.login("admin", "admin");
        admin = userEndpoint.getMyUser();
        assertEquals(3, projectEndpoint.getListAllUsersProjects().size());
    }

    @Test
    public void testDeleteAllProjects() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyProjectException_Exception, EmptyProjectIdException_Exception {
        final ProjectDTO project1 = new ProjectDTO();
        project1.setId(UUID.randomUUID().toString());
        project1.setUserId(admin.getId());
        projectEndpoint.createProject(project1);
        final ProjectDTO project2 = new ProjectDTO();
        project2.setId(UUID.randomUUID().toString());
        project2.setUserId(admin.getId());
        projectEndpoint.createProject(project2);

        assertEquals(2, projectEndpoint.getListProjects().size());
        projectEndpoint.deleteAllProjects();
        assertEquals(0, projectEndpoint.getListProjects().size());
    }

    @Test
    public void testExistsProjectById() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyProjectException_Exception, EmptyProjectIdException_Exception {
        final ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(admin.getId());
        projectEndpoint.createProject(project);

        assertTrue(projectEndpoint.existsProjectById(project.getId()));
        assertFalse(projectEndpoint.existsProjectById("5245rgdf"));
    }

}
