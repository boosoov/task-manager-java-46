package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class TaskEndpointTest {

    @Autowired
    private UserEndpoint userEndpoint;

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private AuthService authService;

    @Autowired
    private TaskEndpoint taskEndpoint;
    
    private UserDTO admin;

    @Before
    public void init() throws EmptyLoginException_Exception, DeniedAccessException_Exception, UnknownUserException_Exception, EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        authService.login("admin", "admin");
        adminUserEndpoint.clearAllUser();
        authService.login("admin", "admin");
        taskEndpoint.deleteAllTasks();
        taskEndpoint.deleteAllTasks();
        admin = userEndpoint.getMyUser();
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, UnknownUserException_Exception, EmptyUserIdException_Exception {
        authService.login("admin", "admin");
        taskEndpoint.deleteAllTasks();
        adminUserEndpoint.clearAllUser();
    }

    @Test
    public void testCreateTask() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyTaskException_Exception {
        final TaskDTO task = new TaskDTO();
        task.setId(UUID.randomUUID().toString());
        task.setUserId(admin.getId());
        task.setName("task");
        taskEndpoint.createTask(task);
        final TaskDTO taskFromServer = taskEndpoint.findTaskByIdDTO(task.getId());
        assertEquals(admin.getId(), taskFromServer.getUserId());
        assertEquals("task", taskFromServer.getName());
    }

    @Test
    public void testFindTaskByIdDTO() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyTaskException_Exception {
        final TaskDTO task = new TaskDTO();
        task.setId(UUID.randomUUID().toString());
        task.setUserId(admin.getId());
        taskEndpoint.createTask(task);
        assertNotNull(taskEndpoint.findTaskByIdDTO(task.getId()));
    }

    @Test
    public void testDeleteTaskById() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyTaskException_Exception, EmptyTaskIdException_Exception {
        final TaskDTO task = new TaskDTO();
        task.setId(UUID.randomUUID().toString());
        task.setUserId(admin.getId());
        taskEndpoint.createTask(task);
        assertNotNull(taskEndpoint.findTaskByIdDTO(task.getId()));
        taskEndpoint.deleteTaskById(task.getId());
        assertNull(taskEndpoint.findTaskByIdDTO(task.getId()));
    }

    @Test
    public void testGetListTasks() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyTaskException_Exception, EmptyTaskIdException_Exception {
        final TaskDTO task1 = new TaskDTO();
        task1.setId(UUID.randomUUID().toString());
        task1.setUserId(admin.getId());
        taskEndpoint.createTask(task1);
        final TaskDTO task2 = new TaskDTO();
        task2.setId(UUID.randomUUID().toString());
        task2.setUserId(admin.getId());
        taskEndpoint.createTask(task2);

        assertEquals(2, taskEndpoint.getListTasks().size());
    }

    @Test
    public void testGetListAllUsersTasks() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyTaskException_Exception, EmptyTaskIdException_Exception {
        final TaskDTO task1 = new TaskDTO();
        task1.setId(UUID.randomUUID().toString());
        task1.setUserId(admin.getId());
        taskEndpoint.createTask(task1);
        final TaskDTO task2 = new TaskDTO();
        task2.setId(UUID.randomUUID().toString());
        task2.setUserId(admin.getId());
        taskEndpoint.createTask(task2);

        authService.login("test", "test");
        admin = userEndpoint.getMyUser();
        final TaskDTO task3 = new TaskDTO();
        task3.setId(UUID.randomUUID().toString());
        task3.setUserId(admin.getId());
        taskEndpoint.createTask(task3);

        authService.login("admin", "admin");
        admin = userEndpoint.getMyUser();
        assertEquals(3, taskEndpoint.getListAllUsersTasks().size());
    }

    @Test
    public void testDeleteAllTasks() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyTaskException_Exception, EmptyTaskIdException_Exception {
        final TaskDTO task1 = new TaskDTO();
        task1.setId(UUID.randomUUID().toString());
        task1.setUserId(admin.getId());
        taskEndpoint.createTask(task1);
        final TaskDTO task2 = new TaskDTO();
        task2.setId(UUID.randomUUID().toString());
        task2.setUserId(admin.getId());
        taskEndpoint.createTask(task2);

        assertEquals(2, taskEndpoint.getListTasks().size());
        taskEndpoint.deleteAllTasks();
        assertEquals(0, taskEndpoint.getListTasks().size());
    }

    @Test
    public void testExistsTaskById() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception, EmptyTaskException_Exception, EmptyTaskIdException_Exception {
        final TaskDTO task = new TaskDTO();
        task.setId(UUID.randomUUID().toString());
        task.setUserId(admin.getId());
        taskEndpoint.createTask(task);

        assertTrue(taskEndpoint.existsTaskById(task.getId()));
        assertFalse(taskEndpoint.existsTaskById("5245rgdf"));
    }

}
