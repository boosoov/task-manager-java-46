package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class AdminEndpointTest {

    @Autowired
    private AdminEndpoint adminEndpoint;

    @Autowired
    private AuthService authService;

    @Before
    public void init() throws EmptyLoginException_Exception, DeniedAccessException_Exception, UnknownUserException_Exception {
        authService.login("admin", "admin");
    }

    @After
    public void clearAll() {
    }

    @Test
    public void testGetHostPort() {
        assertNotNull(adminEndpoint.getHostPort());
    }

//    @Test
//    public void testGetHost() {
//        assertNotNull(adminEndpoint.getHost());
//    }

    @Test
    public void testGetPort() {
        assertNotNull(adminEndpoint.getPort());
    }

}
